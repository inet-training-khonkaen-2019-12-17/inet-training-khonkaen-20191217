<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'author' => $faker->firstName,
        'price' => $faker->numberBetween(100, 1000),
        'describe' => $faker->text,
        'type' => $faker->randomElement([
            'สารคดี', 'นิตสาร', 'บันเทิง',
        ])
    ];
});
