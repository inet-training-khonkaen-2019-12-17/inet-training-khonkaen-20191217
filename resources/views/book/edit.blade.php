@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Book Edit Form <strong> # {{ $book->name }}</strong> </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form action="{{ route('book.edit') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $book->id }}" name="id">
                            <div class="form-group">
                                <label for="name">Name :</label>
                                <input type="text" name="name" value="{{ $book->name }}" class="form-control" id="name" required>
                            </div>
                            <div class="form-group">
                                <label for="author">Author :</label>
                                <input type="text" name="author" value="{{ $book->author }}" class="form-control" id="author" required>
                            </div>
                            <div class="form-group">
                                <label for="describe">Describe:</label>
                                <textarea class="form-control" name="describe" id="describe" rows="3" required>{{ $book->describe }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="type">Type :</label>
                                <select class="form-control" name="type" id="type">
                                    <option
                                        @if($book->type === 'สารคดี') {{ "selected" }}@endif
                                    >สารคดี</option>
                                    <option
                                        @if($book->type === 'บันเทิง') {{ "selected" }}@endif
                                    >บันเทิง</option>
                                    <option
                                        @if($book->type === 'นิตยสาร') {{ "selected" }}@endif
                                    >นิตยสาร</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price">Price :</label>
                                <input type="number" name="price" value="{{ $book->price }}" class="form-control" id="price" required>
                            </div>
                            <div align="right">
                                <a href="{{ route('book.page') }}" class="btn btn-danger">Back</a>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
