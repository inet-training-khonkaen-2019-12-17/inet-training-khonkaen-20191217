@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Main Page</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div align="right" style="margin-bottom: 30px">
                                <a href="{{ route('book.create.page') }}" class="btn btn-secondary">
                                    + Book
                                </a>
                            </div>
                            <table class="table">
                                <thead>
                                <tr align="center">
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Author</th>
                                    <th scope="col">Describe</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Edit</th>
                                    <th scope="col">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($books as $index => $book)
                                    <tr align="center">
                                        <th scope="row">{{ $index + 1 }}</th>
                                        <td>{{ $book->name }}</td>
                                        <td>{{ $book->author }}</td>
                                        <td align="left">{{ $book->describe }}</td>
                                        <td>{{ $book->type }}</td>
                                        <td>{{ $book->price }}</td>
                                        <td>
                                            <a href="{{ route('book.edit.page', $book->id) }}" class="btn btn-warning">
                                                <i class="material-icons" style="font-size: 15px">&#xe895;</i>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ route('book.delete', $book->id) }}" class="btn btn-danger">
                                                <i class="material-icons" style="font-size: 15px">&#xe92b;</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
