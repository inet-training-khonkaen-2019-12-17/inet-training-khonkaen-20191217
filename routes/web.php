<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/book', 'BookController@index')->name('book.page');
Route::group(['middleware' => ['user']], function () {
    Route::get('/book/create', 'BookController@createPage')->name('book.create.page');
    Route::post('/book/create', 'BookController@create')->name('book.create');
    Route::get('/book/edit/{id}', 'BookController@editPage')->name('book.edit.page');
    Route::post('/book/edit', 'BookController@edit')->name('book.edit');
    Route::get('/book/delete/{id}', 'BookController@delete')->name('book.delete');


    Route::post('/logout', 'User\UserController@logout')->name('logout');
    Route::get('/profile', 'User\UserController@profile')->name('profile');
    Route::post('/onechat/store' , 'User\UserController@storeOneChat')->name('onechat.store');
});




Route::get('/register', 'User\UserController@registerPage')->name('register.page');
Route::post('/register', 'User\UserController@register')->name('register');
Route::get('/login', 'User\UserController@loginPage')->name('login.page');
Route::post('/login', 'User\UserController@login')->name('login');


Route::get('/home', function () {
    return view('home');
});
