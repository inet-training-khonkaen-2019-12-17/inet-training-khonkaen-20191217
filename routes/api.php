<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::prefix('v1')->group(function (){
    Route::get('books', 'Api\v1\BookController@index');
    Route::post('books', 'Api\v1\BookController@create');
    Route::get('books/{id}', 'Api\v1\BookController@show');
    Route::delete('books/{id}', 'Api\v1\BookController@destroy');
    Route::put('books/{id}', 'Api\v1\BookController@update');
});
