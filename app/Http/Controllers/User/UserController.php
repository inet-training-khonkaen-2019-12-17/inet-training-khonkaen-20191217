<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Onechat;
use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function registerPage()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $userExist = User::where('email', $request->email)->exists();

        if ($userExist) {
            return redirect()->back()->with(['status' => 'user is already existing!']);
        }

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        if (!$user->save()) {
            return redirect()->back();
        }

        return redirect()->route('login.page');

//        $validatedData = $request->validate([
//            'name' => 'required|max:255',
//            'email' => 'required|email|max:255',
//            'password' => 'required|min:6|confirmed'
//        ]);
//
//        dd($validatedData);
    }

    public function loginPage()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
//        // แบบที่ 1
//        $user = User::where('email', $request->email)
//            ->get()->first();
//        // check error user มีไหม
//
//        if (Hash::check($request->password, $user->password)){
//            // แบบที่ 1.1
//            Auth::login($user);
//
//            // แบบที่ 1.2
//            Auth::loginUsingId($user->id);
//            return redirect('/');
//        }
        // แบบที่ 2
        $isAuth = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password,
        ]);
        if (!$isAuth){
            return redirect()->back()->with(['status' => 'Login failed!']);
        }
        return redirect('/');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function profile()
    {
        $user = Auth::user()->onechat;
        dd($user);
        return view('auth.profile', compact('user'));
    }

    public function storeOneChat(Request $request)
    {
        $info = $this->checkOneChatUser($request->one_mail);
        if ($info->status === 'fail') {
            return redirect()->back()->with(['status' => 'Fail!']);
        }

        $onechat = new Onechat();
        $onechat->one_mail = $request->one_mail;
        $onechat->onechat_id = $info->friend->user_id;
        $onechat->user_id = Auth::user()->id;

        if (!$onechat->save()) {
            return redirect()->back()->with(['status' => 'Save Fail!']);
        }
        $this->sendMessage('สวัสดี คุณได้ทำการแก้ไขโปรไฟล์เรียบร้อยแล้ว',  $info->friend->user_id);
        return redirect()->back();
    }

    private function checkOneChatUser($email)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-manage.one.th:8997/api/v1/searchfriend", [
                "headers" => [
                    'Authorization' => "Bearer A83c82ffd632854faa8d6df99fa883a851f798d62d307436eb773ac2eab3701b65462c40a3951460aa6707f63a1191d4d",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'bot_id' => "B007ad35586535b6a8d08156178f8d174",
                    "key_search" => $email
                ]
            ]);
            $resToJson = json_decode($res->getBody()->getContents());
            return $resToJson;
        } catch (GuzzleException $e) {
           return (object) ['status' => 'fail'];
        }
    }

    private function sendMessage($msg, $onechat_id)
    {
        try {
            $client = new Client();
            $res = $client->request('POST', "https://chat-public.one.th:8034/api/v1/push_message", [
                "headers" => [
                    'Authorization' => "Bearer A83c82ffd632854faa8d6df99fa883a851f798d62d307436eb773ac2eab3701b65462c40a3951460aa6707f63a1191d4d",
                    "Content-Type" => "application/json",
                ],
                'json' => [
                    'to' => $onechat_id,
                    'bot_id' => "B007ad35586535b6a8d08156178f8d174",
                    'type' => 'text',
                    "message" => $msg,
                ]
            ]);
            return null;
        } catch (GuzzleException $e) {
            return null;
        }
    }
}
