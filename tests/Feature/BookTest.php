<?php

namespace Tests\Feature;

use App\Book;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BookTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGuestCanSeeBookPage()
    {
        $response = $this->get(route('book.page'));

        $response->assertViewIs('book.index')
            ->assertStatus(200);
    }

    public function testGuestCanSeeAllBookExist()
    {
        // Arrange
        $books = factory(Book::class, 5)->create();

        // Act
        $this->get( route('book.page'))
            // Assert
            ->assertSee($books[0]->name)
            ->assertSee($books[1]->name)
            ->assertSee($books[2]->name)
            ->assertSee($books[3]->name)
            ->assertSee($books[4]->name);

    }

    public function testGuestCanAddNewBook()
    {
        // Arrange
        $book = factory(Book::class)->make();

        $this->get(route('book.create.page'))
            ->assertViewIs('book.create')
            ->assertStatus(200);

        // Act
        $this->post(route('book.create'), [
            '_token' => \Session::token(),
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ])->assertRedirect(route('book.page'));

        // Assert
        $this->assertDatabaseHas('books', [
            'name' => $book->name,
            'author' => $book->author,
            'price' => $book->price,
            'describe' => $book->describe,
            'type' => $book->type,
        ]);

    }

    public function testGuestCanEditBook()
    {
        // Arrange
        $book = factory(Book::class)->create();
        $newBook = factory(Book::class)->make();

        // Act
        $this->get(route('book.edit.page', $book->id))
            ->assertViewIs('book.edit')
            ->assertSee($book->name)
            ->assertSee($book->price)
            ->assertStatus(200);

        $this->post(route('book.edit'), [
            '_token' => \Session::token(),
            'id' => $book->id,
            'name' => $newBook->name,
            'author' => $newBook->author,
            'price' => $newBook->price,
            'describe' => $newBook->describe,
            'type' => $newBook->type,
        ])->assertRedirect(route('book.page'));

        // Assert
        $this->assertDatabaseHas('books', [
            'name' => $newBook->name,
            'author' => $newBook->author,
            'price' => $newBook->price,
            'describe' => $newBook->describe,
            'type' => $newBook->type,
        ]);
    }

    public function testGuestCanDeleteBook()
    {
        // Arrange
        $book = factory(Book::class)->create();

        // Act
        $this->get(route('book.delete', $book->id))
            ->assertRedirect(route('book.page'));

        // Assert
        $isBook = Book::find($book->id);
        $this->assertEmpty($isBook);

    }
}
